package com.github.twentyEightJan.tez;

public class Main {

    public static void main(String[] args) {

        System.out.println(Tez.tez("Андрей", "Андрей"));
        System.out.println(Tez.tez("Андрей", "Яков"));
        System.out.println(Tez.tez("Яков", "Яков"));

    }
}