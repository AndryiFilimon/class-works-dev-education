package com.github.twentyEightJan.minMaxInt;


public class MaxInt {
    public static int maxInt(int a, int b, int c, int d, int e){
        int [] arr = new int[5];
        arr[0] = a;
        arr[1] = b;
        arr[2] = c;
        arr[3] = d;
        arr[4] = e;
        int max = arr[0];

        for (int i = 0; i < arr.length; i++){
            if (max < arr[i]){
                max = arr[i];
            }
        }
        return max;
    }
}
