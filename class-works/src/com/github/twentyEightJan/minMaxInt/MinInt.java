package com.github.twentyEightJan.minMaxInt;

public class MinInt {
    public static int minInt(int a, int b, int c, int d, int e){
        int [] arr = new int[5];
        arr[0] = a;
        arr[1] = b;
        arr[2] = c;
        arr[3] = d;
        arr[4] = e;
        int min = arr[0];

        for (int i = 0; i < arr.length; i++){
            if (min > arr[i]){
                min = arr[i];
            }
        }
        return min;
    }
}
