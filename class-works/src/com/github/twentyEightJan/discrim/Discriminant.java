package com.github.twentyEightJan.discrim;

public class Discriminant {
    public static void main(String[] args) {
        System.out.println(discriminant(5, 4, 6));
    }
    public static int discriminant(int a, int b, int c){
        return b * b - 4 * a * c;
    }
}