package com.github.twentyEightJan.seasons;

public class Seasons {

    public static void main(String[] args) {
        System.out.println(season(5));
    }

    public static String season (int month){
        String message = "";
        if (month >= 0 && month <= 2){
            message = "Зима";
        }
        else if (month >= 3 && month <= 5){
            message = "Весна";
        }
        else if (month >= 6 && month <= 8){
            message = "Лето";
        }
        else if (month >= 9 && month <= 11){
            message = "Осень";
        }
        else if (month == 12){
            message = "Зима";
        }
        else {
            message = "Неверное число. Введите число от 1 до 12";
        }
        return message;
    }
}
