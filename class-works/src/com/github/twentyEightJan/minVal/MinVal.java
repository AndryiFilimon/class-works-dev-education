package com.github.twentyEightJan.minVal;

public class MinVal {
    public static int minVal(int a, int b, int c, int d){
        int [] arr = new int[4];
        arr[0] = a;
        arr[1] = b;
        arr[2] = c;
        arr[3] = d;
        int min = arr[0];

        for (int i = 0; i < arr.length; i++){
            if (min > arr[i]){
                min = arr[i];
            }
        }
        return min;
    }
}
