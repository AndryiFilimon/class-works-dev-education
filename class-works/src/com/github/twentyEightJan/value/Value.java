package com.github.twentyEightJan.value;

public class Value {

    public static int value(int a, int b, int c, int d){

            int [] arr = new int[4];
            arr[0] = a;
            arr[1] = b;
            arr[2] = c;
            arr[3] = d;
            int max = arr[0];
            int count = 0;

            for (int i = 0; i < arr.length; i++){
                if (max < arr[i]){
                    max = arr[i];
                    count++;
                }
            }
            return count;
        }
    }
