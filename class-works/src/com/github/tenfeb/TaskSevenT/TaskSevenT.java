package com.github.tenfeb.TaskSevenT;

import java.io.File;
import java.io.IOException;

public class TaskSevenT {

    public static boolean isFilenameValid(File file) {
        File f = new File(String.valueOf(file));
        try {
            f.getCanonicalPath();
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}
