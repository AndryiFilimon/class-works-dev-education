package com.github.tenfeb.TaskTw;

import org.junit.Assert;
import org.junit.Test;

public class TaskTwTest {
    @Test
    public void taskTwTest(){
        String test = "C:/Program Files/Internet Explorer/images/bing.ico";
        String exp = "C:\\Program Files\\Internet Explorer\\images\\bing.ico";
        String act = TaskTw.taskTw(test);
        Assert.assertEquals(exp, act);
    }
}
