package com.github.tenfeb.TaskElevenT;

import org.junit.Assert;
import org.junit.Test;

public class TaskElevenTTest {

    @Test

    public void taskElevenTTest(){
        String exp = "Replaces, each, substring, of; this? string that matches the given.";
        String act = TaskElevenT.taskElevenT();
        Assert.assertEquals(exp, act);
    }
}
