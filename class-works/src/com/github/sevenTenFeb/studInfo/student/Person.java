package com.github.sevenTenFeb.studInfo.student;

import java.security.SecureRandom;
import java.util.Objects;

public class Person implements Comparable<Person> {

    private int id;
    private String name;
    private String name2;
    private String name3;
    private int dateBirth;
    private String address;
    private int phone;
    private String faculty;
    private int course;
    private int group;

    public Person(int id, String name, String name2, String name3, int dateBirth, String address, int phone, String faculty, int course, int group) {
        this.id = id;
        this.name = name;
        this.name2 = name2;
        this.name3 = name3;
        this.dateBirth = dateBirth;
        this.address = address;
        this.phone = phone;
        this.faculty = faculty;
        this.course = course;
        this.group = group;
    }

    public Person(String name, String name2, String name3, int dateBirth, String address, int phone, String faculty, int course, int group) {
        SecureRandom secureRandom = new SecureRandom();
        this.id = Math.abs(secureRandom.nextInt());
        this.name = name;
        this.name2 = name2;
        this.name3 = name3;
        this.dateBirth = dateBirth;
        this.address = address;
        this.phone = phone;
        this.faculty = faculty;
        this.course = course;
        this.group = group;
    }

    public Person() {
        this.id = 0;
        this.name = "name";
        this.name2 = "name2";
        this.name3 = "name3";
        this.dateBirth = 0;
        this.address = "address";
        this.phone = 0;
        this.faculty = "faculty";
        this.course = 0;
        this.group = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getName3() {
        return name3;
    }

    public void setName3(String name3) {
        this.name3 = name3;
    }

    public int getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(int dateBirth) {
        this.dateBirth = dateBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id
                && dateBirth == person.dateBirth
                && phone == person.phone
                && course == person.course
                && group == person.group
                && Objects.equals(name, person.name)
                && Objects.equals(name2, person.name2)
                && Objects.equals(name3, person.name3)
                && Objects.equals(address, person.address)
                && Objects.equals(faculty, person.faculty);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, name2, name3, dateBirth, address, phone, faculty, course, group);
    }

    @Override
    public String toString() {
        final StringBuffer sd;
        sd = new StringBuffer("{Person");
        sd.append("id='").append(id).append('\'');
        sd.append(", name='").append(name).append('\'');
        sd.append(", name2='").append(name2).append('\'');
        sd.append(", name3='").append(name3).append('\'');
        sd.append(", dataBirth='").append(dateBirth).append('\'');
        sd.append(", address='").append(address).append('\'');
        sd.append(", phone='").append(phone).append('\'');
        sd.append(", faculty='").append(faculty).append('\'');
        sd.append(", course='").append(course).append('\'');
        sd.append(", group='").append(group).append('\'');
        return toString();
    }

    @Override
    public int compareTo(Person o) {
        return 0;
    }
}
